package com.mkyong.core;

public class Calculatrice {
	
	static public int addition(int a, int b) {
		return a+b;
	}
	static public int soustraction(int a, int b) {
		return a-b;
	}
	static public int multiplication(int a, int b) {
		return a*b;
	}
	static public double division(int a, int b) {
		return a/b;
	}
	static public String retourneString(int a) {
		if(a == 1) {
			return "a";
		}
		else {
			return null;
		}
	}
	static public Boolean retourneBoolean(int a) {
		if(a == 1) {
			return true;
		}
		else {
			return false;
		}
	}
	
	
	static public double calcul(int a, int b,String operateur) {
		double res = 0 ;
		switch(operateur) {
		case "+":
			res = a+b;
			break;
		case "-":
			res = a-b;
			break;
		case "*":
			res = a*b;
			break;
		case "/":
			res = a/b;
			break;
		default:
		break;
		}
		return res;
	}
	
	public static long factorielIteratif(int n) {
        long resultat = 1;
        for (int i = 1; i <= n; i++) {
            resultat *= i;
        }
        return resultat;
    }
	
	public static long factorielRecursif(int n) {
        if (n == 0 || n == 1) {
            return 1;
        } else {
            return n * factorielRecursif(n - 1);
        }
    }
	
	public static boolean attendreTemps(int t) {
		try {
            Thread.sleep(t * 1000); // Convertit les secondes en millisecondes
            return true;
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return false;
        }
	}
	
}
