package com.mkyong.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class GuessTest {
	
	@BeforeAll
    static void beforeAllTests() {
        System.out.println("Début de tous les tests GuessTest");
    }

    @BeforeEach
    void beforeEachTest() {
        System.out.println("Début du test");
    }

    @AfterEach
    void afterEachTest() {
        System.out.println("Fin du test");
    }

    @AfterAll
    static void afterAllTests() {
        System.out.println("Fin de tous les tests GuessTest");
    }
    
    @DisplayName("Test Guess.determineGuess")
    @Test
    void testCalculatriceAdditionAssertEquals() {
		 long startTime = System.currentTimeMillis();
        assertEquals("Correct!\nTotal Guesses: 5", Guess.determineGuess(5,5,5));
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        System.out.println("Temps nécessaire pour testCalculatriceAdditionAssertEquals : " + duration + " ms");
    }

}
