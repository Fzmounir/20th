package com.mkyong.core;



//import static org.assertj.core.api.Assertions.assertThat;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;




public class CalculatriceTest {
	
	@BeforeAll
    static void beforeAllTests() {
        System.out.println("Début de tous les tests CalculatriceTest");
    }

    @BeforeEach
    void beforeEachTest() {
        System.out.println("Début du test");
    }

    @AfterEach
    void afterEachTest() {
        System.out.println("Fin du test");
    }

    @AfterAll
    static void afterAllTests() {
        System.out.println("Fin de tous les tests CalculatriceTest");
    }


	@DisplayName("Test Calculatrice.addition.assertEquals")
    @Test
    void testCalculatriceAdditionAssertEquals() {
		 long startTime = System.currentTimeMillis();
        assertEquals(10, Calculatrice.addition(5, 5));
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        System.out.println("Temps nécessaire pour testCalculatriceAdditionAssertEquals : " + duration + " ms");
    }
	  
	@DisplayName("Test Calculatrice.addition.assertNotEquals")
    @Test
    void testCalculatriceAdditionAssertNotEquals() {
		 long startTime = System.currentTimeMillis();
        assertNotEquals(5, Calculatrice.addition(5, 5));
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        System.out.println("Temps nécessaire pour testCalculatriceAdditionAssertNotEquals : " + duration + " ms");
    }
	
	@DisplayName("Test Calculatrice.retourneString nulle")
    @Test
    void testCalculatriceRetourneStringNull() {
		 long startTime = System.currentTimeMillis();
        assertNull(Calculatrice.retourneString(5));
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        System.out.println("Temps nécessaire pour retourneString null : " + duration + " ms");
    }
	
	@DisplayName("Test Calculatrice.retourneString non null")
    @Test
    void testCalculatriceRetourneStringNotNull() {
		 long startTime = System.currentTimeMillis();
        assertNotNull(Calculatrice.retourneString(1));
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        System.out.println("Temps nécessaire pour retourneString non null : " + duration + " ms");
    }
	
	@DisplayName("Test Calculatrice.retourneBoolean true")
    @Test
    void testCalculatriceRetourneBooleanTrue() {
		 long startTime = System.currentTimeMillis();
        assertTrue(Calculatrice.retourneBoolean(1));
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        System.out.println("Temps nécessaire pour retourneBoolean true : " + duration + " ms");
    }
	
	@DisplayName("Test Calculatrice.retourneBoolean false")
    @Test
    void testCalculatriceRetourneBooleanFalse() {
		 long startTime = System.currentTimeMillis();
        assertFalse(Calculatrice.retourneBoolean(5));
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        System.out.println("Temps nécessaire pour retourneBoolean false : " + duration + " ms");
    }
	
	@DisplayName("Test Calculatrice.addition.fail")
    @Test
    void testCalculatriceAdditionFaile() {
		 long startTime = System.currentTimeMillis();
        if( Calculatrice.addition(5, 5) !=10) {
        	 fail("Le résultat obtenu est incorrect : " + Calculatrice.addition(5, 5));
        }
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        System.out.println("Temps nécessaire pour testCalculatriceAddition fail : " + duration + " ms");
    }
}
