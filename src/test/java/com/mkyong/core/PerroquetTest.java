package com.mkyong.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class PerroquetTest {
	
	@DisplayName("Test Perroquet.perroquetParlant()")
    @Test
    void testPerroquetParlant() {
        assertEquals("Bonjour", Perroquet.perroquetParlant("Bonjour"));
    }
	
	@DisplayName("Test Perroquet.perroquetBonjour()")
    @Test
    void testPerroquetBonjour() {
        assertEquals("Bonjour toto", Perroquet.perroquetBonjour("toto"));
    }
}
